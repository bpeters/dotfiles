set nocompatible
set showmatch		" Show matching brackets
set ignorecase
set mouse=v
set hlsearch
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set autoindent
syntax on
filetype plugin indent on
set noshowmode

