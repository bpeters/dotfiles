# Gimme dat nvim for editing
export EDITOR='nvim'

# Aliases
alias ls='ls --color'

## Alias to allow config file tracking
alias dotfile='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Starship
eval "$(starship init bash)"
